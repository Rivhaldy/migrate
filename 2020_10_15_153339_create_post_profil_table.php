<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostProfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_profil', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('nama_lengkap');
            $table->string('email',45);
            $table->string('foto',45);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_profil', function (Blueprint $table){
        $table->dropForeign(['profil_id']);
    });
    }
}
