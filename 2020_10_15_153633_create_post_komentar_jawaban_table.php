<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_komentar_jawaban', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('isi', 45);
            $table->date('tanggal_dibuat');
             $table->foreign('komentar_jawaban_id')->references('id')->on('post_jawaban');
             $table->foreign('profil_id')->references('id')->on('post_profil');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_komentar_jawaban', function(Blueprint $table){

        $table->dropForeign(['komentar_jawaban_id']);
        $table->dropForeign(['profil_id']);
    });
    }
}
