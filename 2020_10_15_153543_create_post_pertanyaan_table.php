<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_pertanyaan', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('judul', 45);
            $table->string('isi', 45 );
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diperbaruhi');
             $table->foreign('pertanyaan_jawaban_id')->references('id')->on('post_jawaban');
             $table->foreign('profil_id')->references('id')->on('post_profil');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_pertanyaan', function (Blueprint $table){

        $table->dropForeign(['pertanyaan_jawaban_id']);
        $table->dropForeign(['profil_id']);

    });
    }
}
