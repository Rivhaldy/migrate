<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_like_dislike_pertanyaan', function (Blueprint $table) {
             $table->bigInteger('pertanyaan_id');
             $table->bigInteger('profil_id');
             $table->foreign('pertanyaan_id')->references('id')->on('post_pertanyaan');
             $table->foreign('profil_id')->references('id')->on('post_profil');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_like_dislike_pertanyaan', function (Blueprint $table){
    
        $table->dropForeign(['pertanyaan_id']);
        $table->dropForeign(['profil_id']);


    });
}
}
