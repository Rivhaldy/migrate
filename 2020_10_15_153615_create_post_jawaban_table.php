<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_jawaban', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('isi', 45);
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diperbaruhi');
              $table->foreign('jawaban_id')->references('id')->on('post_pertanyaan');
             $table->foreign('profil_id')->references('id')->on('post_profil');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_jawaban', function (Blueprint $table){

        $table->dropForeign(['jawaban_id']);
        $table->dropForeign(['profil_id']);

    });
    }
}
