<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_like_dislike_jawaban', function (Blueprint $table) {
             $table->bigInteger('jawaban_id');
             $table->bigInteger('profil_id');
             $table->foreign('jawaban_id')->references('id')->on('post_jawaban');
             $table->foreign('profil_id')->references('id')->on('post_profil');
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_like_dislike_jawaban', function(Blueprint $table){
        $table->dropForeign(['jawaban_id']);
        $table->dropForeign(['profil_id']);
    });
    }
}
